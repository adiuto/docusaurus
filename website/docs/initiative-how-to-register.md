# How an initiative is registered

To register an initiative, all you need is a user account:

https://adiuto.org/user

Then a new initiative can be registered under the following link:

https://adiuto.org/initiatives

<img src="/assets/ini-register.de.jpg" alt="Register initiative" />

Registration requires a title of the initiative, a description and a [location](initiative-administration.md#locations) for fundraising.
An image (landscape format) can also be uploaded.

Then you can start adding [Tasks](initiative-tasks.md).