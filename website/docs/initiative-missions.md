# Missions

Missions are created by users of adiuto.org and are collections of tasks they have undertaken.

Missions go through four application steps:

1. **Planning:** Users browse adiuto.org and collect tasks in their mission.
2. **Checkout:** Mission planning completes.
3. **Pending:** The mission has been checked out and donations are being delivered to [drop-off location](initiative-administration.md#locations) by donors (other tasks will be performed accordingly).
4. **Completed:** Donations and other tasks have been submitted or completed and [confirmed](initiative-introduction.md#3-drop-off).


## Coordinate missions

All missions for an initiative can be viewed in the right menu under *Coordinate Missions*.

<img src="/assets/ini-menuMissions.en.png" alt="Coordinate Missions" />

> The number highlighted in blue shows the number of [pending](#missions) missions.

All missions for a specific task can be displayed directly with the task itself or under [coordinate tasks](initiative-tasks.md#tasks-coordinate).