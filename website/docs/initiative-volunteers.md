# How to engage your volunteers

## Accept donations

Members accepting donations should have a basic understanding of how adiuto.org is used.

The following four points suffice:

- People have selected *tasks* (donations) on adiuto.org and collected them in a *mission*.

- People who come to the initiative to complete tasks (make donations) have received a QR code for their mission (by email or on adiuto.org) and have it on their smartphone.

- **TODO: The QR code must be scanned with a smartphone and the link displayed at the end of the list must be clicked.**

- Visiting the link completes the mission for the initiative and the person.

<img src="/assets/QRscanWithPhone-initiative-x480.jpg" alt="Scan QR Code" />

## Set and coordinate tasks

Each member who is entrusted with the administration of tasks must be assigned the [Coordination](initiative-administration.md#members) role.

All opportunities for this role are described on the [Tasks](initiative-tasks.md) page.

This task requires a certain training period and an overview of the need for donations and should therefore be taken on by members with a longer-term commitment.

In addition, coordinators can also take over the supervision of the [P2P Module](initiative-p2p.md). However, this task should only be assigned to members who have already gained experience in coordinating tasks.
