# How to use Adiuto

You have become aware of adiuto.org and are wondering how you can get started with the platform?
Or you are a representative of a company that wants to donate and wonder how Platform can support you.
<!--adiuto.org is a needs-based platform and the usage scenarios are the same for both individuals and larger companies.-->

adiuto.org is used by initiatives to organize donation needs and you can select those you wish to take on.

adiuto.org ensures that you know what is really needed.

## Task

Anything an initiative needs support is labeled as a **task** on adiuto.org. Most of the time, these are donations (e.g. hygiene items, food or electrical items) that are handed over to the initiative. For each task there is a requirement that shows how much is required.

<img src="/assets/don-task.de.png" alt="Task" />

If there is currently no need for a cause Su would like to donate, you can create a [offer list](#givelist) and adiuto.org will notify you when there is a need.

### That's how it's done

- If you know which initiative you want to get involved in, then go to the initiative's page (e.g. via the overview [of all initiatives](https://adiuto.org/initiatives)). There you can view all the tasks of the initiative.
- You can also view [all tasks](https://adiuto.org/tasks) and use the search box (above) and filters (below) to find what you can donate.

## Mission

Mission are collections of tasks. The shopping cart icon in the top menu bar always takes you to your current mission, here you can change the number of donations or remove tasks.

<img src="/assets/don-topMenu.de.png" alt="Top Menu" />

### That's how it's done

1. **Collect tasks** that you want to take on. To do this, click on the shopping cart symbol next to a task.<br />Done? Then ...
2. **open your mission** and customize it.<br />Done? Then click on *Continue to checkout*.<br /><img src="/assets/don-proceedToCheckout.de.png" alt="Continue to checkout" />

3. **Checkout:** If you are not registered, we will ask for your email at this point.<br />Ready? Then click on *Check out your mission and commit it*. You will receive an email with all the information and a QR code needed to complete the mission.<br /><img src="/assets/don-checkout.de.png" alt="Checkout" />

4. Now you can take the tasks / donations you have taken on to the drop-off point of the initiative.
5. **Complete missions:** At the drop-off location, you hand over your donations to a member of the initiative. Now all thats left to do is scan the QR code to complete the mission.

You can find an overview of all missions under *My missions* on your [profile](#account).

## User account {#account}

If you create a user account on adiuto.org, you have the possibility to easily view and manage your missions on the platform and to save your donation offers in the [offer list](#givelist) so that we can inform you if something is needed becomes.

However, you can also use adiuto.org without registering. You only have to enter your e-mail when checking out a mission.

### Registration

1. On [adiuto.org](https://adiuto.org) you get to the registration/login via the person symbol (top right).
2. Select *I want to create an account* here
3. Fill in all fields and click *Create new user account*.
   > The first and last name are not mandatory fields! They will be displayed instead of your username if you provide them.
4. Then click on *Create new user account*.
5. You will receive an email with a link to confirm your registration. If you click on the link, the registration is complete.

Alternatively, you can also log in with your Facebook, Google or Twitter account. To do this, click on the appropriate button at the end of the form.

## Givelist {#givelist}

> Can only be used with a [user account](#account).

Donation offers can be saved in the give-list.

adiuto.org searches all tasks at regular intervals (standard: once a day) for the saved search term with the selected filters and displays the number of tasks found on the give-list.

### Saving offers

On the task list the search box can be used to find a matching need. If there is no suitable task, the search query can be saved with the *Save search to givelist* button. All filters set (e.g. location or initiative) are also saved.

The latest searches are displayed on your [profile](https://adiuto.org/user).