# Coordinate P2P

## Introduction

With P2P peer-to-peer, individual needs can be registered by initiatives and fulfilled by donors.

<img src="/assets/adiuto-p2pProcess.en.png" alt="" />

## 1. Gather requests and needs

People with a need come to the initiative and a member of the initiative writes down the request and an e-mail address, or enters the data straight away on adiuto.org.

#### Example

A person needs a stroller and reports the need of the initiative. A member of the initiative notes down the request *stroller* and the email address *name@example.com* of the person.

## 2. Enter request on adiuto.org

### Create P2P request

If a [task](initiative-tasks.md) with the appropriate request already exists (e.g. because another person has already made the same request), a P2P request (P2P reference) can be created directly will. Otherwise, a suitable [task created](initiative-tasks.md#tasks-add) must first be created and only then can the P2P request be created.

1. Call up the P2P coordination page in the right menu.
<img src="/assets/ini-menuP2P.de.png" alt="P2P Coordinate" />

2. Click on the **+ ADD P2P** button.
<img src="/assets/p2p-AddP2PButton.png" alt="" />

3. In the **Task reference** field, type the beginning of the task you are looking for and select the appropriate entry (e.g. *(3526) Baby carriage - x* from the list. (If no suitable term is displayed, a suitable task can be created.)
<img src="/assets/p2p-AddTask.png" alt="" />
  - ✓ This task is active and can be selected without any problems (to be on the safe side, you can check how long the task is still active)
  - x This task is deactivated, but can still be selected (but should be activated after creation).

4. Enter the email address in the **Email** field (e.g. *name@example.com*).

5. The **status** should remain on *required* when creating, it will be changed automatically in the further process.

### Edit task

To edit a task after creating the P2P request (e.g. to activate it), you can search for it in the overview page using the task ID (e.g. *3526*) and edit it by clicking on EDIT will.

<img src="/assets/p2p-AddTaskEdit.png" alt="" />

> Important: The demand of a task referenced by P2P requests is automatically adjusted so that it always matches the number of P2P requests. As long as at least one active P2P request refers to a task, its requirement can no longer be changed manually.

<img src="/assets/p2p-Requests.png" alt="" />

### Practical tips

- When creating new P2P requests, existing tasks should be used if possible. It can be helpful to adapt existing tasks and thus make them usable for other requests as well, as long as this does not significantly change the content.

- When searching for tasks, it is best to only search for *keywords* (and possibly German / English) so that suitable tasks that are only formulated differently can be found.

- It should also be checked whether a similar request for the email address (same person) does not already exist.

- Formulate tasks as unambiguously as possible (e.g. *pram* vs. *sibling's stroller*).

- The "Coordinate tasks" function in the right menu is also helpful for processing the tasks.


## 3. Helpers can find the request on adiuto.org

For helpers there is no difference when [using adiuto.org](donor-how-to.md). The task is added to a mission and then brought to initiative.

Because the requests can be very individual and in order to keep an overview when assigning the donation, the following note is automatically placed in the description when a task is created:

> Note: Please note the number of this task (3526) on the article.

This makes it easier to match the donation to the P2P request when it's picked up.

Even if helpers check out a mission, the need for the task is reduced accordingly. That is, as long as the mission is on the way, the need of the task is 1 less than the number of P2P requests. Only when the mission has reached the initiative (*delivered*) is the number of P2P requests automatically reduced by 1.

## 4. Helpers bring the donations to the initiative

### Receive donation / complete mission

When the donations are handed over, the [mission QR code is scanned](initiative-introduction.md#3-drop-off) as usual. The QR code leads to a page with a button to complete the mission. Clicking the button also causes the status of the P2P request to be changed from *required* to *delivered*.

> There can also be several P2P requests for a task (e.g. if several people need a stroller). The request that was first entered in the system is always processed first.

### Notify the requester

A green letter icon now appears next to the corresponding task in the list of P2P requests. Clicking sends a notification email to the email address entered there.

<img src="/assets/p2p-notification.png" alt="" />

Explanation of the overview of P2P requests (P2P References): The list can be sorted by most columns, which makes it e.g. B. Easier to find all P2P requests from a specific email address.

<img src="/assets/p2p-filters.png" alt="" />

## 5. Hand over the donations

The person can now go to the initiative and pick up the donation. The notification email also contains a QR code for identification. This code needs to be scanned by a member of the initiative and links to a page with the associated request and a button to confirm collection. Clicking this button changes the status of the P2P request (P2P Reference) from *delivered* to *fetched*.