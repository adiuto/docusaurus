# Introduction for initiatives

This is how adiuto.org works

<img src="/assets/adiuto-process.en.png" alt="" />

## 1. Create tasks

Tasks are e.g. the donation of hygiene articles or clothing or the transport of relief supplies. [Tasks created](initiative-tasks.md#tasks) are created on adiuto.org for the different things that an initiative determines as needed.

### Demand

Each task has a demand that indicates how often that task is needed.

## 2. Plan missions

Missions are collections of tasks created by donors. Comparable to a shopping cart, donors add tasks to a mission.

Once a mission is checked out, the initiative is notified that the mission is on its way and the [demand](#demand) is reduced accordingly to account for ongoing donations. If a mission is abandoned, the original demand is restored.

## 3. Drop off

After checking out, donors will pack up all donations and head to the specified [location](initiative-administration.md#locations) to drop them off.

Each mission has a QR code that can be found along with the mission details on adiuto.org (under *My Missions*) or in the email sent after checkout.
At the location, a [member](initiative-administration.md#members) of the initiative can scan the QR code with their smartphone and click on the link that is displayed. Clicking on the link completes and ends the mission. <!--(The member must be [member](initiative-administration.md#members) of the initiative on adiuto.org and logged in.)-->

<img src="/assets/QRscanWithPhone-initiative-x480.jpg" alt="" />