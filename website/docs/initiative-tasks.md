# Tasks

For the different things that an initiative needs, tasks are created on adiuto.org. Tasks are e.g. B. the donation of hygiene articles or clothing, or the transport of relief supplies.

### State
Tasks can have a status of *active* or *deactivated*. Tasks will not appear in task lists if they are disabled or if their demand is 0.

### Delete
Tasks can only be deleted if they were never part of a mission. Otherwise they can only be deactivated.

## Add tasks

<img src="/assets/ini-menuAdd.de.png" alt="Add Tasks" />

Tasks can be created by invoking **Add Task* in the right menu.

- **Title:** A descriptive title, this will be displayed in the task lists.

- **Location:** This sets the location where the task can be completed. [Locations](initiative-administration.md#locations) can be created when editing the initiative.

- **Requirement:** The requirement for the task is specified here. The need is reduced when donors complete missions with the corresponding task.

- **Recurring demand:** Here you can specify whether the demand should be reset to its initial value after the selected period.

### Practice tip
Demand is a very important variable and should be estimated as realistically as possible. It is motivating for donors when their donation has a visible impact on the need. adiuto.org works particularly well in a dynamic process in which the need for donations is actively managed and a static list is not artificially generated.

- **Description:** A short description that will be shown to donors when they access the task.

- **Categories:** Categories can be selected here. Donors can filter the task lists by these categories.

- **Deadline:** The time until which this task will remain active.

## Add multiple tasks

<img src="/assets/ini-menuAddMulti.de.png" alt="Add multiple tasks" />

In the right menu under *++ Add multiple tasks* multiple tasks can be created via a text field or CSV import. Please note the information under *Import Options* on the page.

## coordinate tasks {#coordinate}

<img src="/assets/ini-menuTasks.de.png" alt="Coordinate tasks" />

In the right menu under *Coordinate tasks* an overview of all tasks can be displayed.

> The number highlighted in yellow shows the amount of tasks with a deadline shorter than **2** days.

- To find specific task, you can use the search box.
- It can be filtered by status: *active*, *deactivated*, or *-All-*.
- With the filters in the right menu, the tasks can also be filtered according to all other criteria, as in the task lists.
- All [missions](initiative-missions.md) for a task can be displayed.
- Multiple tasks can be processed at the same time. Multiple tasks can be selected using the check boxes and different properties can be selected and adjusted via *Operation* -> *Change values*.

<img src="/assets/ini-tasksCoordination.de.png" alt="Coordinate tasks" />