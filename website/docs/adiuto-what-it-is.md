# What Adiuto is and what it is not

## What Adiuto is

adiuto.org lists needs for donations with the desired amount, urgency and the location of the need. This way you can easily find out where your help or donation is needed. Initiatives can publish needs-based demand for donations that show accurately and specificly what is needed. The donation supply chain can also be tracked in this way.

The support that an initiative needs, e.g. specifically donations, is called a *task*, and multiple tasks that a person promises to accomplish are called a *mission*.

### For initiatives

We support initiatives and organizations in telling potential donors exactly what is needed, in what quantity, when and where.
Join adiuto.org to easily and efficiently manage your donation supply chain.

## What Adiuto is not

adiuto.org is not suitable for listing donation offers. We believe that need is a much better criterion for what is seen as necessary. In this way, the commitment made by members of the initiative, donors and volunteers is purposeful and meaningful.

However, with the [offer list](donor-how-to.md#givelist), adiuto.org offers the possibility of being informed when a certain task is needed.