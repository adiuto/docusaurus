# Manage initiative, location and members

On an initiative's page, all management links are in the right menu. From there, [create tasks](initiative-tasks.md), edit the initiative, and manage members.

## initiative {#edit}

The initiative's information can be edited using the *Edit Initiative* shortcut in the right menu. Only [administrators](#members) can edit initiatives.

<img src="/assets/ini-menuEdit.de.png" alt="Edit Initiative" />

Each initiative needs at least one [location](#locations) to [create tasks](initiative-tasks.md). The locations are [collection points](initiative-introduction.md#3-drop-off) for donations.

Information about the initiative can be found in the *Description* field, opening hours should be entered directly at the respective location under *Details* (this is how they are displayed with each [mission](initiative-missions.md)).

## Locations {#locations}

Locations are places to [raise funds](initiative-introduction.md#3-drop-off) and in adiuto.org each task is associated with a location where it can be completed. An initiative can have multiple locations, but if the requested donations differ too much between locations, it's best to create another initiative instead of having multiple locations:

- If locations are managed together and the donations made are collected centrally, these locations should be part of a single initiative.

- If locations are managed separately and the collected donations are needed directly at the location, an initiative should be created for each location.

Locations are created and edited in the initiative edit form.

<img src="/assets/ini-editLocations.png" alt="" />

## members {#members}

There are three roles for members of an initiative:

- **Members** can complete incoming missions (to do this, [scan the QR code](initiative-introduction.md#3-drop-off) if the donations arrive successfully).
- **Coordinators** can [create](initiative-tasks.md#add-tasks) and [edit] all tasks for their initiative(initiative-tasks.md#coordinate-tasks). Coordinators can also create and edit [P2P requests](initiative-p2p.md).
- **Admins** can additionally change the role of members, accept new ones and remove members from the initiative. You can also edit the [Initiative](#initiative) page and the [Locations](#locations).

The person who creates an [initiative](initiative-how-to-register.md) receives the *Administration* role for that initiative.

<!--People who join an existing initiative are initially members with the status *Pending*. The status must be set to *Activated* by administrators. This can be done on one of the member sites.-->

There are two pages for managing members:

1. For all members, in addition to *About us* and *Tasks*, a *Members* page is displayed with a list of all members organized by role.
2. For admins, there is a *Manage Members* shortcut in the *right menu*, with a more detailed overview and more options.

<img src="/assets/ini-menuMembers.de.png" alt="Manage Members" />

Administrators can change **Status** and **Role** of members by clicking the pencil icon.

<img src="/website/static/assets/ico-pencil.png" alt="pen icon" />