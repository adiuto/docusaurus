---
title: Get started
slug: /
---

> Das adiuto.org Benutzerhandbuch auf **Deutsch**: https://docs.adiuto.org/de


Welcome to the adiuto.org Documentation. You can find all the topics in the left menu.
The English language version is under construction and there is not a lot of content.

