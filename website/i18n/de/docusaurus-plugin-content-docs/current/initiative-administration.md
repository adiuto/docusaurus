# Initiative, Standort und Mitglieder verwalten

Auf der Seite einer Initiative befinden sich im rechten Menü alle Verwaltungslinks. Von dort aus werden [Aufgaben erstellt](initiative-tasks.md), die Initiative bearbeitet und Mitglieder verwaltet.

## Initiative {#edit}

Die Informationen der Initiative können mit der Verknüpfung *Initiative bearbeiten* im rechten Menü bearbeitet werden. Nur [Administrierenden](#members) können Initiativen bearbeiten.

<img src="/assets/ini-menuEdit.de.png" alt="Initiative bearbeiten" />

Jede Initiative benötigt mindestens einen [Standort](#locations) um [Aufgaben anzulegen](initiative-tasks.md). Die Standorte sind [Sammelstellen](initiative-introduction.md#3-drop-off) für Spenden.

Im Feld *Beschreibung* kann über die Initiative informiert werden, Öffnungszeiten sollten direkt beim jeweiligen Standort unter *Details* eingegeben werden (so werden sie bei jeder [Mission](initiative-missions.md) mit angezeigt).

## Standorte {#locations}

Standorte sind Orte zum [Sammeln von Spenden](initiative-introduction.md#3-drop-off) und in adiuto.org ist jede Aufgabe einem Standort zugeordnet an dem sie abgeschlossen werden kann. Eine Initiative kann mehrere Standorte haben, aber wenn sich die angeforderten Spenden zwischen den Standorten zu sehr unterscheiden, ist es am besten, eine weitere Initiative zu erstellen, anstatt mehrere Standorte zu haben:

- Wenn Standorte gemeinsam verwaltet werden und die abgegebenen Spenden zentral gesammelt werden, sollten diese Standorte Teil einer einzigen Initiative sein.
    > Dabei ist zu beachten, dass eine Aufgabe kann nur einem einzigen Standort zugeordnet werden kann.

- Wenn Standorte separat verwaltet werden und die gesammelten Spenden direkt am Standort benötigt werden, sollte für jeden Standort eine Initiative angelegt werden. 

Standorte werden im Bearbeitungsformular der Initiative erstellt und bearbeitet.

<img src="/assets/ini-editLocations.png" alt="" />

## Mitglieder {#members}

Es gibt drei Rollen für Mitglieder einer Initiative:

- **Mitglieder** können eingehende Missionen abschließen (dazu den [QR-Code scannen](initiative-introduction.md#3-drop-off), wenn die Spenden erfolgreich ankommen).
- **Koordinierende** können [Aufgaben erstellen](initiative-tasks.md#add) und alle Aufgaben für ihre Initiative [bearbeiten](initiative-tasks.md#coordinate). Koordinierende können auch [P2P Anfragen](initiative-p2p.md) erstellen und bearbeiten.
- **Administrierende** können zusätzlich die Rolle von Mitgliedern ändern, neue akzeptieren und Mitglieder aus der Initiative entfernen. Außerdem können sie die Seite der [Initiative](#initiative) und die [Standorte](#locations) bearbeiten.

Die Person die eine [Initiative anlegt](initiative-how-to-register.md), erhält die Rolle *Administration* für diese Initiative.

<!--Personen die einer bestehenden Initiative beitreten sind zunächst Mitglieder mit dem Status *Ausstehend*. Der Status muss von Administrierenden auf *Aktiviert* gesetzt werden. Dies kann auf einer der Mitgliederseiten vorgenommen werden.-->

Für die Verwaltung der Mitglieder gibt es zwei Seiten:

1. Für alle Mitglieder wird neben *Über uns* und *Aufgaben* auch eine Seite *Mitglieder* angezeigt, mit einer Liste aller Mitglieder nach Rollen geordnet.
2. Für Administratoren gibt es im *rechten Menü* die Verknüpfung *Mitglieder verwalten*, mit einer ausführlicheren Übersicht und mehr Optionen.

<img src="/assets/ini-menuMembers.de.png" alt="Mitglieder verwalten" />

Administratoren können durch Anklicken des Stiftsymbols **Status** und **Rolle** der Mitglieder ändern.

<img src="/website/static/assets/ico-pencil.png" alt="Stiftsymbol" />