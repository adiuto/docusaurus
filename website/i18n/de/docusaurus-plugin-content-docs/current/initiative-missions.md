# Missionen {#missions}

Missionen werden von Benutzenden von adiuto.org angelegt und sind Sammlungen von Aufgaben die diese sich vorgenommen haben.

Missionen gehen durch vier Anwendungsschritte:

1. **Planung:** Benutzende durchstöbern adiuto.org und sammeln Aufgaben in ihrer Mission.
2. **Checkout:** Die Planung der Mission wird abgeschlossen.
3. **Ausstehend:** Die Mission wurde ausgecheckt und Spenden werden von den Spendenden zum [Abgabeort](initiative-administration.md#standorte) gebracht (andere Aufgabe werden entsprechend ausgeführt).
4. **Abgeschlossen:** Spenden und andere Aufgaben wurden abgegeben bzw. abgeschlossen und [bestätigt](initiative-introduction.md#3-drop-off).


## Missionen koordinieren

Alle Mission für eine Initiative können im rechten Menü unter *Missionen koordinieren* angezeigt werden.

<img src="/assets/ini-menuMissions.de.png" alt="Missionen koordinieren" />

> Die blauunterlegte Zahl zeigt die Anzahl der [ausstehenden](#missions) Missionen.

Alle Missionen zu einer bestimmten Aufgabe, können direkt bei der Aufgabe selber oder unter [Aufgaben koordinieren](initiative-tasks.md#coordinate) angezeigt werden.
