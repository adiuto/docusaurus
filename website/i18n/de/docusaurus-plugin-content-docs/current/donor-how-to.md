# Wie wird Adiuto verwendet

Du bist auf adiuto.org aufmerksam geworden und fragst dich wie du mit Hilfe der Platform loslegen kannst?
Oder du bist Vertreter eines Unternehmens das spenden will und fragst dich wie dich Platform dabei unterstützen kann. 
<!--adiuto.org ist eine bedarfsorientierte Platform und die Benutzungsszenarien sind gleich sowohl für Einzelperson als auch für größere Unternehmen.-->

adiuto.org wird von Initiativen benutzt, um den Bedarf an Spenden zu organisieren und Du kannst  diejenigen auswählen, die Du übernehmen möchtest.

adiuto.org sorgt so dafür, dass Du weißt was wirklich gebraucht wird.

## Aufgabe {#task}

Alles was eine Initiative an Unterstützung benötigt, wird auf adiuto.org als **Aufgabe** bezeichnet. Meistens handelt es sich dabei um Spenden (z.B. Hygieneartikel, Nahrungsmittel oder Elektroartikel) die bei der Initiative abgegeben werden. Zu jeder Aufgabe gibt es einen Bedarf, der anzeigt wie viel benötigt wird.

<img src="/assets/don-task.de.png" alt="Aufgabe" />

Wenn gerade kein Bedarf für eine Sache besteht, die Su spenden möchtest, kannst du eine [Angebotsliste](#givelist) anlegen und adiuto.org benachrichtigt dich, sobald Bedarf besteht.

### So geht's

- Wenn Du weißt für welche Initiative du dich engagieren willst, dann gehe zur Seite der Initiative (z.B. über die Übersicht [aller Initiativen](https://adiuto.org/initiatives)). Dort kannst du dir alle Aufgaben der Initiative anzeigen lassen.
- Du kannst Dir auch [alle Aufgaben](https://adiuto.org/tasks) anzeigen lassen und das Suchfeld (oben) und die Filter (unten) benutzen, um dass zu finden, was du spenden kannst. 

## Mission {#mission}

Mission sind Sammlungen von Aufgaben. Das Einkaufswagensymbol in der oberen Menüleiste führt dich immer zu deiner aktuellen Mission, hier kannst du die Anzahl der Spenden ändern oder Aufgaben entfernen.

<img src="/assets/don-topMenu.de.png" alt="Top Menü" />

### So geht's

1. **Sammle Aufgaben** die Du übernehmen möchtest. Klicke dazu auf das Einkaufwagensymbol bei einer Aufgabe.<br />Fertig? Dann ...
2. **öffne deine Mission** und passe sie an.<br />Fertig? Dann klicke auf *Weiter zum Checkout*.<br /><img src="/assets/don-proceedToCheckout.de.png" alt="Weiter zum Checkout" />

3. **Checkout:** Wenn Du nicht registriert bist, fragen wir Dich an dieser Stelle nach Deiner E-Mail.<br />Fertig? Dann klicke auf *Checke Deine Mission aus und betätige sie*. Du erhältst eine E-Mail mit allen Informationen und einem QR Code der zum abschließen der Mission benötigt wird wird.<br /><img src="/assets/don-checkout.de.png" alt="Checkout" />

4. Jetzt kannst du die übernommenen Aufgaben / Spenden zum Abgabeort der Initiative bringen.
5. **Missionen abschließen:** Am Abgabeort übergibst Du deine Spenden einem Mitglied der Initiative. Jetzt muss nur noch der QR Code gescannt werden um die Mission vollständig abzuschließen.

Eine Übersicht aller Missionen findest Du unter *Meine Missionen* auf deinem [Profil](#account).

## Benutzerkonto {#account}

Wenn Du auf adiuto.org eine Benutzerkonto anlegst, hast Du die Möglichkeit Deine Missionen auf der Platform bequem einzusehen und zu verwalten und die Möglichkeit Deine Spendenangebote in der [Angebotsliste](#givelist) zu speichern, so dass wir dich informieren können wenn etwas gebraucht wird.

Du kannst adiuto.org aber auch ohne Registrierung verwenden, Du musst nur beim Checkout einer Mission deine E-Mail angeben.

### Registrierung

1. Auf [adiuto.org](https://adiuto.org) kommst Du über das Personensymbol (oben rechts) zur Registrierung/Anmeldung. 
2. Hier *Ich möchte ein Konto erstellen* auswählen
3. Alle Felder ausfüllen und *Neues Benutzerkonto erstellen* anklicken.
   > Der Vor- und Nachname sind keine Pflichtfelder! Sie werden anstatt deines Benutzernamens angezeigt, wenn Du sie angibst.
4. Anschließend *Neues Benutzerkonto erstellen* anklicken.
5. Du erhältst eine E-Mail mit einem Link um deine Registrierung zu bestätigen. Wenn Du den Link anklickst, ist die Registrierung abgeschlossen.

Alternativ kannst du dich auch mit deinem Facebook-, Google- oder Twitterkonto einloggen. Dazu auf die passende Schaltfläche am Ende des Formulars klicken.

## Angebotsliste {#givelist}

> Die Verwendung ist nur mit [Benutzerkonto](#account) möglich.

In der Angebotsliste können Spendenangebote gespeichert werden.

adiuto.org durchsucht alle Bedarfe in regelmäßigen Abständen (standart: einmal pro Tag) nach dem gespeicherten Suchbegriff mit den ausgewählten Filtern und zeigt die Anzahl der gefundenen Bedarfe auf der Angebotsliste an.

### Speichern von Angeboten

Das Suchfeld der Aufgabenliste kann dazu verwendet werden, einen passenden Bedarf zu finden. Wenn kein passender Bedarf dabei ist, kann die Suchanfrage mit der Schaltfläche *Suche in Meine Angebote speichern* gespeichert werden. Alle gesetzten Filter (z.B. Standort oder Initiative) werden mit gespeichert.

Die letzten Suchanfragen werden auf Deinem [Profil](https://adiuto.org/user) angezeigt.