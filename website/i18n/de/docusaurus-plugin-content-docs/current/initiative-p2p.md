# P2P koordinieren

## Einführung

Mit P2P Peer-to-Peer können individuelle Bedarfe von Initiativen erfasst und von Spendenden erfüllt werden.

<img src="/assets/adiuto-p2pProcess.en.png" alt="" />

## 1. Anfragen und Bedürfnisse sammeln

Personen mit einem Bedürfnis kommen zur Initiative und ein Mitglied der Initiative notiert die Anfrage sowie eine E-Mail-Adresse, oder trägt die Daten gleich in adiuto.org ein.

#### Beispiel

Eine Person benötigt einen Kinderwagen und meldet den Bedarf der Initiative. Ein Mitglied der Initiative notiert die Anfrage *Kinderwagen* und die E-Mail-Adresse *name@example.com* der Person.

## 2. Anfrage auf adiuto.org eintragen

### P2P-Anfrage erstellen

Wenn schon eine [Aufgabe (Task)](initiative-tasks.md) mit der passenden Anfrage vorhanden ist (z. B. weil eine andere Person schon die gleiche Anfrage gestellt hat), kann direkt eine P2P-Anfrage (P2P Reference) erstellt werden. Ansonsten muss erst eine passende [Aufgabe angelegt](initiative-tasks.md#aufgaben-hinzufügen) werden und erst dann kann die P2P-Anfrage erstellt werden.

1. Im rechten Menü die P2P Koordinationsseite aufrufen.
<img src="/assets/ini-menuP2P.de.png" alt="P2P Koordinieren" />

2. Auf die Schaltfläche **+ ADD P2P** klicken.
<img src="/assets/p2p-AddP2PButton.png" alt="" />

3. In das **Task reference** Feld den Anfang der gesuchten Aufgabe eintippen und den passenden Eintrag (z. B. *(3526) Kinderwagen - x* aus der Liste auswählen. (Wenn kein passender Begriff angezeigt wird, muss erst eine passende Aufgabe erstellt werden.)
<img src="/assets/p2p-AddTask.png" alt="" />
  - ✓ Diese Aufgabe ist aktiv und kann problemlos ausgewählt werden (zur Sicherheit kann überprüft werden, wie lange die Aufgabe noch aktiv ist)
  - x Diese Aufgabe ist deaktiviert, kann aber trotzdem ausgewählt werden (sollte aber nach dem Anlegen aktiviert werden ).

4. In das **E-Mail** Feld die E-Mail-Adresse eintragen (z. B. *name@example.com*).

5. Der **Status** sollte beim Anlegen auf *required* bleiben, er wird im weiteren Verlauf automatisch verändert.

### Aufgabe bearbeiten

Um eine Aufgaben nach dem Anlegen der P2P Anfrage zu bearbeiten (z. B. um sie zu aktivieren), kann sie mit der Hilfe der Task ID (z. B. *3526*) in der Übersichtsseite gesucht und mit einem klick auf EDIT bearbeitet werden.

<img src="/assets/p2p-AddTaskEdit.png" alt="" />

> Wichtig: Der Bedarf einer Aufgabe, die von P2P-Anfragen referenziert ist, wird automatisch angepasst, so dass er immer mit der Anzahl der P2P-Anfragen übereinstimmt. Solange mindestens eine aktive P2P-Anfrage auf eine Aufgabe verweist, kann deren Bedarf nicht mehr manuelle geändert werden. 

<img src="/assets/p2p-Requests.png" alt="" />

### Praxis-Tipps

- Beim Anlegen von neuen P2P-Anfragen sollten möglichst vorhandene Aufgaben (Tasks) genutzt werden. Es kann hilfreich sein, bestehende Aufgaben anzupassen und damit auch für andere Anfragen nutzbar zu machen, soweit dadurch der Inhalt nicht maßgeblich verändert wird.

- Beim Suchen nach Aufgaben am besten nur nach *Stichworten* (und ggf. deutsch / englisch) suchen, damit passende Aufgaben, die nur anders formuliert sind, gefunden werden.

- Es sollte auch geprüft werden, ob eine ähnliche Anfrage für die E-Mail-Adresse (gleiche Person) nicht bereits vorhanden ist.

- Aufgaben möglichst unmissverständlich formulieren (z. B. *Kinderwagen* vs. *Geschwisterkinderwagen*).

- Für die Bearbeitung der Aufgaben ist auch die Funktion "Aufgaben koordinieren" im rechten Menü hilfreich.


## 3. Helfende finden die Anfrage auf adiuto.org

Für Helfende ergibt sich bei der [Nutzung von adiuto.org](donor-how-to.md) kein Unterschied. Die Aufgabe wird einer Mission hinzugefügt und dann zur Initiative gebracht.

Weil die Anfragen sehr individuell sein können und um beim Zuordnen der Spende den Überblick zu behalten, wird beim Anlegen einer Aufgabe automatisch der folgende Hinweis in die Beschreibung gesetzt:

> Hinweis: Bitte die Nummer dieser Aufgabe (3526) auf dem Artikel notieren.

So kann die Spende leichter der P2P-Anfrage zugeordnet werden, wenn sie abgeholt wird.

Schon wenn Helfende eine Mission auschecken, wird der Bedarf bei der Aufgabe entsprechend reduziert. D. h., solange die Mission unterwegs ist, ist der Bedarf der Aufgabe um 1 geringer als die Anzahl der P2P-Anfragen. Erst wenn die Mission bei der Initiative angekommen ist (*delivered*), wird automatisch auch die Anzahl der P2P-Anfragen um 1 reduziert.

## 4. Helfende bringen die Spenden zur Initiative

### Spende in Empfang nehmen / Mission abschließen

Bei der Übergabe der Spenden wird wie üblich der [QR-Code der Mission gescannt](initiative-introduction.md#3-drop-off). Der QR-Code führt auf eine Seite mit einer Schaltfläche, um die Mission abzuschließen. Das Klicken der Schaltfläche sorgt auch dafür, dass der Status der P2P-Anfrage von *required* auf *delivered* gesetzt wird.

> Zu einer Aufgabe kann es auch mehrere P2P-Anfragen geben (z. B. wenn mehrere Personen einen Kinderwagen benötigen). Es wird immer diejenige Anfrage zuerst bearbeitet, die auch zuerst im System eingetragen wurde.

### Anfragende Person benachrichtigen

In der Liste der P2P-Anfragen erscheint jetzt ein grünes Briefsymbol bei der entsprechenden Aufgabe. Das Anklicken versendet eine Benachrichtigungsmail an die dort eingetragene E-Mail-Adresse.

<img src="/assets/p2p-notification.png" alt="" />

Erklärung zur Übersicht der P2P-Anfragen (P2P References): Die Liste kann nach den meisten Spalten sortiert werden, das macht es z. B. einfacher, alle P2P-Anfragen von einer bestimmten E-Mail-Adresse zu finden.

<img src="/assets/p2p-filters.png" alt="" />

## 5. Die Spenden übergeben

Die Person kann sich nun zur Initiative begeben und die Spende abholen. Zur Identifikation enthält die Benachrichtigungsmail auch einen QR-Code. Dieser Code muss von einem Mitglied der Initiative gescannt werden und verweist auf eine Seite mit der dazugehörigen Anfrage und einer Schaltfläche, um die Abholung zu bestätigen. Durch das Anklicken dieser Schaltfläche wird der Status der P2P-Anfrage (P2P Reference) von *delivered* auf *fetched* gesetzt.
