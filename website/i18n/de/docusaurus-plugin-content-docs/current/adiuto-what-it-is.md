# Was Adiuto ist und was es nicht ist

## Was Adiuto ist

adiuto.org listet Spendenaufrufe mit gewünschter Höhe, Dringlichkeit und dem Ort des Bedarfs auf. So kannst Du leicht herausfinden, wo Deine Hilfe oder Deine Spende benötigt wird. Initiativen können bedarfsgerechte Spendenaufrufe veröffentlichen, die konkret und aktuell aufzeigen, was gebraucht wird. Außerdem kann so die Spendenlieferkette nachverfolgt werden.

Die Unterstützung die eine Initiative benötigt, insbesondere das Spenden wird als Aufgabe bezeichnet und mehrere Aufgaben, die eine Person zu erfüllen verspricht, werden als Mission bezeichnet.

### Für Initiativen

Wir unterstützen Initiativen und Organisationen dabei, möglichen Spender:innen genau mitzuteilen, was, in welcher Menge, zu welcher Zeit und an welchem Ort benötigt wird.
Tritt adiuto.org bei, um Deine Lieferkette für Spenden einfach und effizient zu verwalten.

## Was Adiuto nicht ist

adiuto.org ist nicht dazu geeignet Spendenangebote zu listen. Wir glauben, dass der Bedarf ein viel besserer Kriterium dafür ist, was als nötig gesehen wird. Der Einsatz den Mitglieder der Initiative, Spendende und freiwillig Helfende leisten, ist auf diese Weise zielgerichtet und bedeutsam.

Allerdings bietet adiuto.org mit der [Angebots-Liste](donor-how-to.md#givelist), die Möglichkeit sich informieren zu lassen, wenn eine bestimmte Aufgabe eingestellt wird.