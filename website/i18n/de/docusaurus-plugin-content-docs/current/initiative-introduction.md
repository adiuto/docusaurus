# Einführung für Initiativen

So funktioniert adiuto.org

<img src="/assets/adiuto-process.en.png" alt="" />

## 1. Aufgaben erstellen

Aufgaben sind z. B. das Spenden von Hygieneartikel oder Kleidung oder der Transport von Hilfsgütern. Für die unterschiedlichen Sachen die eine Initiative benötigt, werden auf adiuto.org [Aufgaben erstellt](initiative-tasks.md#add).


### Bedarf {#demand}

Jede Aufgabe hat einen Bedarf, der angibt, wie oft diese Aufgabe benötigt wird.

## 2. Missionen planen

Missionen sind Sammlungen von Aufgaben die von Spendenden erstellt werden. Vergleichbar mit einem Warenkorb fügen Spendende Aufgaben einer Mission hinzu.

Sobald eine Mission ausgecheckt wurde, wird die Initiative benachrichtigt, dass die Mission unterwegs ist und der [Bedarf](#demand) entsprechend reduziert, um die laufenden Spenden zu berücksichtigen. Wenn eine Mission abgebrochen wird, wird der ursprüngliche Bedarf wiederhergestellt.

## 3. Drop off

Nach dem Auschecken packen die Spendenden alle Spenden zusammen und macht sich auf den Weg zum angegebenen [Standort](initiative-administration.md#locations), um sie abzugeben.

Jede Mission hat einen QR-Code, der zusammen mit den Missionsdetails auf adiuto.org (unter Meine Missionen) oder in der E-Mail zu finden ist, die nach dem Auschecken verschickt wurde.
Am Standort kann ein [Mitglied](initiative-administration.md#members) der Initiative den QR-Code mit dem Smartphone scannen und auf den angezeigten Link klicken. Durch Klicken auf den Link wird die Mission abgeschlossen und beendet. <!--(Das Mitglied muss [Mitglied](initiative-administration.md#members) der Initiative auf adiuto.org und eingeloggt sein.)-->

<img src="/assets/QRscanWithPhone-initiative-x480.jpg" alt="" />