# Wie eine Initiative registriert wird

Um eine Initiative anzumelden, wird lediglich ein Benutzerkonto benötigt:

https://adiuto.org/user

Dann kann unter dem folgendem Link eine neue Initiative angemeldet werden:

https://adiuto.org/initiatives

<img src="/assets/ini-register.de.jpg" alt="Initiative registrieren" />

Für die Anmeldung wird eine Titel der Initiative, eine Beschreibung und ein [Standort](initiative-administration.md#locations zum Sammeln von Spenden benötigt.
Ein Bild (Querformat) kann auch hochgeladen werden.

Anschließend kann damit begonnen werden [Aufgaben](initiative-tasks.md) hinzu zu fügen.