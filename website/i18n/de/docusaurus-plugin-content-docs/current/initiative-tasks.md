# Aufgaben

Für die unterschiedlichen Sachen die eine Initiative benötigt, werden auf adiuto.org Aufgaben erstellt. Aufgaben sind z. B. das Spenden von Hygieneartikel oder Kleidung, oder der Transport von Hilfsgütern.

### Status
Aufgaben können den Status *aktiv* oder *deaktiviert* haben. Aufgaben werden nicht in den Aufgabenlisten angezeigt wenn sie deaktiviert sind oder wenn ihr Bedarf 0 ist.

### Löschen
Aufgaben können nur gelöscht werden, wenn sie nie Teil einer Mission waren. Ansonsten können sie nur deaktiviert werden.

## Aufgaben hinzufügen {#add}

<img src="/assets/ini-menuAdd.de.png" alt="Aufgaben hinzufügen" />

Aufgaben können im rechten Menü durch Aufrufen von *+ Aufgabe hinzufügen* erstellt werden.

- **Titel:** Ein aussagekräftiger Titel, dieser wird in den Aufgabenlisten angezeigt.

- **Ort:** Hier wird der Ort festgelegt, an dem die Aufgabe abgeschlossen werden kann. [Standorte](initiative-administration.md#standorte) können beim Bearbeiten der Initiative angelegt werden.

- **Bedarf:** Hier wird der Bedarf für die Aufgabe festgelegt. Der Bedarf reduziert sich wenn Spendende Mission mit der entsprechenden Aufgabe abschließen.

- **Wiederkehrender Bedarf:** Hier kann festgelegt werden, ob der Bedarf nach dem ausgewählten Zeitraum wieder auf seinen Anfangswert gesetzt werden soll.

### Praxis Tip
Der Bedarf ist eine sehr wichtige Größe und sollte möglichst realistisch abgeschätzt werden. Für Spendende ist es motivierend, wenn ihre Spende einen sichtbaren Einfluss auf den Bedarf hat. adiuto.org funktioniert besonders gut in einem dynamischen Prozess in der der Spendenbedarf aktiv gemanagt wird und nicht künstlich eine statische Liste erzeugt wird.

- **Beschreibung:** Eine kurze Beschreibung die den Spendenden beim Aufrufen der Aufgabe angezeigt wird.

- **Kategorien:** Hier können Kategorien ausgewählt werden. Spendende können die Aufgabenlisten nach diesen Kategorien filtern.

- **Frist:** Der Zeitpunkt bis zu dem diese Aufgabe aktiv bleibt.

## Mehrere Aufgaben hinzufügen

<img src="/assets/ini-menuAddMulti.de.png" alt="Mehrere Aufgaben hinzufügen" />

Im rechten Menü können unter *++ Mehrere Aufgaben hinzufügen* mehrere Aufgaben über ein Textfeld oder CSV Import angelegt werden. Bitte dazu die Hinweise unter *Import Options* auf der Seite beachten.

## Aufgaben koordinieren {#coordinate}

<img src="/assets/ini-menuTasks.de.png" alt="Aufgaben koordinieren" />

Im rechten Menü kann unter *Aufgaben koordinieren* eine Übersicht aller Aufgaben angezeigt werden.

> Die gelbunterlegte Zahl zeigt die Anzahl der Aufgaben, deren Frist in den nächsten **zwei** Tagen abläuft.

- Um bestimmte Aufgabe zu finden, kann das Suchfeld verwenden werden.
- Es kann nach dem Status gefiltert werden: *aktiv*, *deaktiviert*, oder *-Alle-*.
- Mit den Filtern im rechten Menü können die Aufgaben auch nach allen anderen Kriterien, wie in den Aufgabenlisten, gefiltert werden.
- Es können alle [Missionen](initiative-missions.md) zu einer Aufgabe anzeigen werden.
- Es können mehrere Aufgaben gleichzeitig bearbeiten werden. Mit den Kontrollkästchen können mehrere Aufgaben ausgewählt werden und über *Operation* -> *Werte ändern* können unterschiedliche Eigenschaften ausgewählt und angepasst werden.

<img src="/assets/ini-tasksCoordination.de.png" alt="Aufgaben koordinieren" />