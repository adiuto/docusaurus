# Wie Sie Ihre Mitglieder einbinden

## Spenden annehmen

Mitglieder, die Spenden annehmen, sollten ein grundlegendes Verständnis dafür haben, wie adiuto.org verwendet wird.

Die folgenden vier Punkte genügen:

- Spendenbedarfe der Initiative werden auf adiuto.org gelistet und Personen wählen auf adiuto.org aus, wass sie Spenden möchten. Die ausgewählten Spenden bzw. Aufgaben werden als *Mission* bezeichnet.

- Personen, die zum Abgaben von Spenden (Abschließen von Aufgaben) zur Initiative kommen, haben einen QR-Code für ihre Mission erhalten (per E-Mail oder auf adiuto.org) und auf ihrem Smartphone dabei.

- **TODO: Der QR-Code muss mit einem Smartphone gescannt und der am Ende der Liste angezeigte Link angeklickt werden.**

- Das Aufrufen des Links schließt die Mission für die Initiative und die Person ab.

<img src="/assets/QRscanWithPhone-initiative-x480.jpg" alt="QR-Code scannen" />

## Aufgaben erstellen und koordinieren

Jedem Mitglied, das mit der Verwaltung von Aufgaben betraut werden soll, muss die Rolle [Koordination](initiative-administration.md#members) zugewiesen werden.

Alle Möglichkeiten für diese Rolle sind auf der Seite [Aufgaben](initiative-tasks.md) beschrieben.

Diese Aufgabe erfordert eine gewisse Einarbeitungszeit und einen Überblick über den Spendenbedarf und sollte daher von Mitgliedern mit längerfristigem Engagement übernommen werden.

Darüber hinaus können Koordinatoren auch die Betreuung des [P2P-Moduls] (initiative-p2p.md) übernehmen. Diese Aufgabe sollte jedoch nur an Mitglieder vergeben werden, die bereits Erfahrung in der Koordinierung von Aufgaben gesammelt haben.