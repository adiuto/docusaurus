# Documentation for adiuto.org

## Entry points
en: https://docs.adiuto.org \
de: https://docs.adiuto.org/de

There is a language switcher on the frontpage and in the sidebar (under **<- Back to main menu**).

## Files
en: https://gitlab.com/adiuto/docusaurus/-/tree/master/website/docs \
de: https://gitlab.com/adiuto/docusaurus/-/tree/master/website/i18n/de/docusaurus-plugin-content-docs/current

### Assets

 - all assets go into `static/assets/` and will be available at `/assets/` after building.

 - only a html link will work (don't forget the trailing slash): `<img src="/assets/NameOfFile.jpg" alt="" />`.

 - no markdown, no relative links (although that is supported by docusaurus, it will not build on gitlab)

 -  ~~For Vercel it works as expected: Images should be placed in `docs/img` and for i18n in `.../current/img`. You can use markdown syntax `![Alternative Text](img/NameOfFile.jpg)`.~~

> For convenience all assets from `/website/static/assets` should be copied to `/assets` to show the images when viewing the markdown file in gitlab.

## Note

- use the normal **Edit** mode. (do not use the Web IDE)

- you can rightclick the editor window and select *Preview Mardown*, to split the window and show the preview on the right.

### New files

 - new files need be created with the same filename for each language in their respective directory (listed above).

 - after adding new files the sidebar needs to be edited to include the new file names https://gitlab.com/adiuto/docusaurus/-/blob/master/website/sidebars.js

 - when adding new categories (structural entities) to sidebar.js, they need to be translated in the current.json file for each language.
